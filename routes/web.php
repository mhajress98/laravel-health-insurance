<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'service',
    'as' => 'service.'

],function(){
    Route::get('/','ServiceController@index')->name('index');
    Route::get('/create','ServiceController@create')->name('create');
    Route::post('/store','ServiceController@store')->name('store');
    Route::get('/show/{service}','ServiceController@show')->name('show');
    Route::get('/edit/{service}','ServiceController@edit')->name('edit');
    Route::post('/update/{service}','ServiceController@update')->name('update');
    Route::get('/destroy/{service}','ServiceController@destroy')->name('destroy');
});

Route::group([
    'prefix' => 'doctor',
    'as' => 'doctor.'

],function(){
    Route::get('/','DoctorController@index')->name('index');
    Route::get('/create','DoctorController@create')->name('create');
    Route::post('/store','DoctorController@store')->name('store');
    Route::get('/ajax_res/{doctor}','DoctorController@ajax_res')->name('respond');
    Route::get('/show/{doctor}','DoctorController@show')->name('show');
    Route::get('/edit/{doctor}','DoctorController@edit')->name('edit');
    Route::post('/update/{doctor}','DoctorController@update')->name('update');
    Route::get('/destroy/{doctor}','DoctorController@destroy')->name('destroy');
});

Route::group([
    'prefix' => 'company',
    'as' => 'company.'

],function(){
    Route::get('/','CompanyController@index')->name('index');
    Route::get('/create','CompanyController@create')->name('create');
    Route::post('/store','CompanyController@store')->name('store');
    Route::get('/show/{company}','CompanyController@show')->name('show');
    Route::get('/edit/{company}','CompanyController@edit')->name('edit');
    Route::post('/update/{company}','CompanyController@update')->name('update');
    Route::get('/destroy/{company}','CompanyController@destroy')->name('destroy');
});

Route::group([
    'prefix' => 'patient',
    'as' => 'patient.'

],function(){
    Route::get('/','PatientController@index')->name('index');
    Route::get('/create','PatientController@create')->name('create');
    Route::post('/store','PatientController@store')->name('store');
    Route::get('/show/{patient}','PatientController@show')->name('show');
    Route::get('/edit/{patient}','PatientController@edit')->name('edit');
    Route::post('/update/{patient}','PatientController@update')->name('update');
    Route::get('/destroy/{patient}','PatientController@destroy')->name('destroy');
});

Route::group([
    'prefix' => 'invoice',
    'as' => 'invoice.'

],function(){
    Route::get('/','InvoiceController@index')->name('index');
    Route::get('/create/{patient}','InvoiceController@create')->name('create');
    Route::post('/store','InvoiceController@store')->name('store');
    Route::get('/show/{invoice}','InvoiceController@show')->name('show');
    Route::get('/edit/{id}','InvoiceController@edit')->name('edit');
    Route::post('/update/{id}','InvoiceController@update')->name('update');
    Route::get('/destroy/{invoice}','InvoiceController@destroy')->name('destroy');
});
