<?php

use Illuminate\Database\Seeder;
use App\Company;
use Illuminate\Support\Facades\Hash;


class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = new Company;
        $company->name = "لا يتبع شركة";
        $company->user_id = 1;
        $company->cover_percentage = 0;
        $company->save();
    }
}
