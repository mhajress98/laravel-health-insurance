<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->password = Hash::make("secret");
        $user->save();

        $user = new User;
        $user->name = 'مستخدم محذوف';
        $user->email = 'deleted@deleted.com';
        $user->password = Hash::make('secret');
        $user->save();

    }
}
