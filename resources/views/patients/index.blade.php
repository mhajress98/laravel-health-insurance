@extends('layouts.app')

@section('title')
    المرضى
@endsection
@section('content')

<div class="container card" dir="rtl">
    <table class="table table-striped table-bordered" style="margin-bottom: 0px; margin-top: 10px;">
        <tr>
            <td align="center"><strong>#</strong></td>
            <td align="center"><strong>الإسم</strong></th>
            <td align="center"><strong>العمر</strong></td>
            <td align="center"><strong>الرقم التسلسلي</strong></td>
            <td align="center"><strong>أُضيف بواسطة</strong></td>
            <td align="center"><strong>الجنس</strong></td>
            <td align="center"><strong>فصيلة الدم</strong></td>
            <td align="center"><strong>الشركة الذي يتبعها</strong></td>
            <td align="center"><strong>عمليات</strong></td>
        </tr>
        @forelse ($patients as $patient)

                    <tr>
                        <td align="center">{{ ++$loop->index }}</td>
                        <td align="center">{{ $patient->name }}</td>
                        <td align="center">{{ $patient->age }}</td>
                        <td align="center">{{ $patient->serial_number }}</td>
                        <td align="center">{{ $patient->user->name }}</td>
                        <td align="center">{{ $patient->gender }}</td>
                        <td align="center">{{ $patient->blood_type}}</td>
                        <td align="center">{{ $patient->company->name}}</td>

                        <td align="center">
                            <a href="{{ route('patient.edit',['patient'=>$patient->id]) }}" class=" btn btn-warning">تعديل</a>
                            <a href="{{ route('patient.destroy',['patient'=>$patient->id]) }}" class="btn btn-danger" onclick="return confirm('هل تريد أن تحذف هذا المريض ؟')">حذف</a>
                            <a href="{{ route('invoice.create',['patient'=>$patient->id]) }}" class=" btn btn-primary">إصدار فاتورة</a>
                        </td>
                    </tr>

                @empty
                    <tr colspan="4">لا يوجد مرضى</tr>
            @endforelse
        </table>
        <hr>
        <div class="row">
                <div class="col-2">
                        <a href="{{ route('patient.create') }}" class="btn btn-primary" style="margin-bottom: 10px;" >إضافة مريض</a>
                </div>
        </div>
</div>
@endsection
