@extends('layouts.app')

@section('title')
    تعديل مريض
@endsection

@section('content')
    <div class="container card" dir="rtl">
        <div class="card-header">تعديل مريض</div>
            <div class="card-body">
            <form action="{{ route('patient.update',['id'=>$patient->id]) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-4">
                <label for="name" class="float-right">إسم المريض</label>
                <input type="text" name="name" id="name" placeholder="أدخل اسم المريض" value="{{ $patient->name }}" class="form-control">
                @if ($errors->has('name'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
                    </div>

                    <div class="col-4">
                    <label for="age" class="float-right"> عمر المريض</label>
                    <input type="text" name="age" id="age" placeholder="أدخل عمر المريض" value="{{ $patient->age }}" class="form-control">
                    @if ($errors->has('age'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('age') }}</strong>
                        </span>
                    @endif
                    </div>
                </div>
                <hr>
                <div class="row">

                    <div class="col-2">
                <label for="gender" class="float-right">الجنس</label>
                <select name="gender" class="form-control">
                    <option disabled selected value> -- إختر الجنس -- </option>
                    <option value="ذكر">ذكر</option>
                    <option value="أنثى">أنثى</option>
                </select>
                @if ($errors->has('gender'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                    </div>

                    <div class="col-2">
                    <label for="blood_type" class="float-right"> فصيلة الدم</label>
                    <select name="blood_type" class="form-control">
                        <option disabled selected value> -- إختر فصيلة دم -- </option>
                        <option value="A+">A+</option>
                        <option value="O+">O+</option>
                        <option value="B+">B+</option>
                        <option value="AB+">AB+</option>
                        <option value="A-">A-</option>
                        <option value="O-">O-</option>
                        <option value="B-">B-</option>
                        <option value="AB-">AB-</option>
                      </select>
                      @if ($errors->has('blood_type'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('blood_type') }}</strong>
                        </span>
                    @endif
                    </div>

                    <div class="col-4">
                    <label for="company" class="float-right"> الشركة التي يتبعها</label>

                        <select name="company" class="form-control">
                            @foreach ($companies as $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                        </select>
                        <span class="float-right alert-warning">
                                <strong>إذا لم يتم اختيار شركة , سيتم اعتبار المريض بأنه لا يتبع أي شركة </strong>
                            </span>
                    </div>
                </div>
            </div>
            <hr>
                <div class="col-2">
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                <input type="submit" value="حفظ" class="form-control btn btn-primary">
                </div>

            </form>
            </div>
    </div>
@endsection
