@extends('layouts.app')

@section('title')
    Medical Sys
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        You are logged in!
                        <hr>
                                          <div class="container">
                                              <div class="row">
                                                  <div class="col-md-4 col-xl-4">
                                                      <div class="card bg-c-blue order-card">
                                                          <div class="card-block">
                                                              <h3 class="m-b-20">الخدمات</h3><h3 class="m-b-20">{{$services}}</h3>
                                                              <i class="fa fa-medkit" style="font-size:48px;"></i><br><br>
                                                              <a href="{{ route('service.index') }}"><button type="button" class="btn btn-outline-light">عرض الكل</button></a>

                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 col-xl-4">
                                                      <div class="card bg-c-green order-card">
                                                          <div class="card-block">
                                                              <h3 class="m-b-20">الشركات</h3><h3 class="m-b-20">{{$companies}}</h3>
                                                              <i class="fa fa-building-o" style="font-size:48px;"></i><br><br>
                                                              <a href="{{ route('company.index') }}"><button type="button" class="btn btn-outline-light">عرض الكل</button></a>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 col-xl-4">
                                                      <div class="card bg-c-yellow order-card">
                                                          <div class="card-block">
                                                              <h3 class="m-b-20">الأطباء</h3><h3 class="m-b-20">{{$doctors}}</h3>
                                                              <i class="fa fa-user-md" style="font-size:48px;"></i><br><br>
                                                              <a href="{{ route('doctor.index') }}"><button type="button" class="btn btn-outline-light">عرض الكل</button></a>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 col-xl-6">
                                                      <div class="card bg-c-pink order-card">
                                                          <div class="card-block">
                                                              <h3 class="m-b-20">المرضى</h3><h3 class="m-b-20">{{$patients}}</h3>
                                                              <i class="fa fa-heartbeat" style="font-size:48px;"></i><br><br>
                                                              <a href="{{ route('patient.index') }}"><button type="button" class="btn btn-outline-light">عرض الكل</button></a>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 col-xl-6">
                                                      <div class="card bg-c-gray order-card">
                                                          <div class="card-block">
                                                              <h3 class="m-b-20">الفواتير</h3><h3 class="m-b-20">{{$invoices}}</h3>
                                                              <i class="fa fa-file-text" style="font-size:48px;"></i><br><br>
                                                              <a href="{{ route('invoice.index') }}"><button type="button" class="btn btn-outline-light">عرض الكل</button></a>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
