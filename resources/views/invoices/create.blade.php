@extends('layouts.app')

@section('title')
    فاتورة جديدة
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                       إصدار فاتورة  : {{ $patient->name }}
                </div>
                    <div class="card-body">
                       <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <form id="form" action="{{ route('invoice.store') }}" method="POST">
                                    @csrf
                                <table class="table table-bordered" dir="rtl">
                                         <span class="float-right alert-warning">
                                            <strong>يجب تعبئة كل الحقول في الفاتورة ليتم إصدار الفاتورة</strong>                                            </strong><br>
                                         </span>
                                         <span class="float-left alert-info">
                                             <strong>+ لإضافة عنصر للفاتورة إضغط زر </strong><br>
                                         </span>

                                    <thead>
                                        <tr>
                                            <th>الطبيب</th>
                                            <th>الخدمة</th>

                                            <th> <a href="#" class="btn btn-info addRow" style="color:aliceblue;">+</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>
                                {{-- @if ($errors->has('doctor') ||  $errors->has('service') || $errors->has('payment_method')) --}}
                                    <span class="float-right alert-danger" id="erm">
                                        {{-- <strong>لقد تركت حقل فارغ</strong> --}}
                                    </span>
                                {{-- @endif --}}
                                <div class="col-4">
                                <select name="payment_method" class="form-control">
                                         <option disabled selected value> -- إختر طريقة الدفع -- </option>
                                         @foreach ($payment_methods as $method)
                                         <option value="{{$method->id}}">{{$method->method}}</option>
                                         @endforeach
                                </select>
                                </div>
                                <hr>
                                <div class="col-2">

                                        <input type="hidden" name="patient_id" value="{{$patient->id}}">
                                        <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                        <input type="submit" id="save" value="إصدار" class="form-control btn btn-primary">
                                </div>
                            </form>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var k = 0;
        $('.addRow').on('click', function(){
            addRow();
        });

        function addRow() {

            var tr = '<tr>'+
                '<td><select name="doctor[]" data-slide ="'+k+'" class="doc form-control">'+
                        '<option disabled selected value> -- إختر طبيب -- </option>'+
                        '@foreach ($doctors as $doctor)'+
                        '<option value="{{$doctor->id}}">{{$doctor->name}}</option>'+
                        '@endforeach'+
                    '</select>'+
                '</td>'+
                // id="'+'ser'+k+'"
                '<td><select id="ser" data-slide ="'+k+'" name="service[]" class="serve form-control">'+
                        '<option disabled selected value> -- الخدمة -- </option>'+
                    '</select>'+
                '</td>'+
                '<td> <a href="#" class="btn btn-danger remove">-</a></td>'+
            '</tr>';
            $('tbody').append(tr);
            k++;
        };

        $('tbody').on('click', '.remove', function(){
            $(this).parent().parent().remove();
        });

        $(document).on('change','.doc',function(){
            // var x = $(this).attr('data-slide').val();

            var x = $(this).attr('data-slide');
            $(".serve[data-slide='"+x+"']").children().remove();
            var route =  "{{ route('doctor.respond',['id' => 'val']) }}"
                $.ajax({
                    type:'GET',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: route.replace('val',$(this).find(':selected').val()),
                    data: null,
                    success: function(response){

//console.log(response.length);
//console.log(response[0].name);
                    //console.log(response);
                   // console.log(x);

                        var len = 0;
                        if (response != null) {
                            len = response.length;
                        }

                        if (len > 0) {

                            for (var i = 0; i < len; i++) {

                                var id = response[i].id;
                                var name = response[i].name;

                                var option = "<option value ='" + id + "'>" + name + "</option>";
                               //"'+'ser'+k+'"
                            //    $('p[MyTag="Sara"]')
                               $(".serve[data-slide='"+x+"']").append(option);
                                // $().append(option);
                            }
                        }

                    },
                    error: function (response) {
                        alert("fail");
                    }
                });

            });

            $(document).ready(function(){
                $('#save').click(function() {
                    event.preventDefault();
                    var data = $('#form').serialize();
                    // alert(data);
                    var route =  "{{ route('invoice.store') }}";

                $.ajax({
                    type : 'POST',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: route,
                    data: data,
                    success: function(response){
                        if($('tbody').children().length == 0){
                            $('#erm').html('يجب إضافة عنصر واحد على الأقل للفاتورة');
                        }
                        else {
                        if(response.error){
                            // errors = response.error;
                            console.log(response.error);
                            $('#erm').html(response.error[0]);
                            // error['payment_method']
                        }
                        else {
                            // Swal.fire('تم إصدار الفاتورة بنجاح',
                            //            'success' )
                            Swal.fire(
                                'OK!',
                                'تم إصدار الفاتورة بنجاح',
                                'success'
                            )
                            setTimeout(function(){ window.location = '{{ route('invoice.index') }}'; }, 3000);
                        }

                        }
                        // alert(response.error);
                    },
                    error: function(e){
                        console.log(e);
                    }


                 });
                })
            })
    </script>
@endsection
