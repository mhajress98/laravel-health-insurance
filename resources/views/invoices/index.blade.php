@extends('layouts.app')

@section('title')
    الفواتير
@endsection
@section('content')

<div class="container card" dir="rtl">
    <table class="table table-striped table-bordered" style="margin-bottom: 0px; margin-top: 10px;">
        <tr>
            <td align="center"><strong>#</strong></td>
            <td align="center"><strong>الرقم التسلسلي</strong></td>
            <td align="center"><strong>اسم المريض</strong></td>
            <td align="center"><strong>الشركة التي يتبعها</strong></td>
            <td align="center"><strong>إجمالي الفاتورة</strong></td>
            <td align="center"><strong>طريقة الدفع</strong></td>
            <td align="center"><strong>أُضيف بواسطة</strong></td>
            <td align="center"><strong>عمليات</strong></td>
        </tr>
        <?php $total = 0; ?>
        @forelse ($invoices as $invoice)

                    <tr>
                        <td align="center">{{ ++$loop->index }}</td>
                        <td align="center">{{ (++$loop->index)+520087654 }}</td>
                        {{-- <td align="center">{{ $invoice->created_at->diffInSeconds($invoice->patient->updated_at)*100 }}</td> --}}
                        <td align="center">{{ $invoice->patient->name }}</td>
                        <td align="center">{{ $invoice->patient->company->name}}</td>
                        {{-- <select name="blood_type" class="form-control">
                            <option disabled selected value> --اضغط لعرض الخدمات-- </option>
                            @foreach ($invoice->invoice_items->services as $service)
                                <option value>{{ $invoice->invoice_items->service }}</option>
                            @endforeach
                        </select> --}}
                        <?php $total += ($invoice->total) - (($invoice->total)*($invoice->patient->company->cover_percentage/100)) ?>
                        <td align="center">{{ ($invoice->total) - (($invoice->total)*($invoice->patient->company->cover_percentage/100)) }} د.ل</td>
                        <td align="center">{{ $invoice->payment_method->method }}</td>
                        <td align="center">{{ $invoice->user->name}}</td>

                        <td align="center">
                            <a href="{{ route('invoice.destroy',['invoice'=>$invoice->id]) }}" class="btn btn-danger" onclick="return confirm('هل تريد أن تحذف هذه الفاتورة ؟')">حذف</a>
                        </td>
                    </tr>

                @empty
                    <tr colspan="4">لا يوجد فواتير</tr>
            @endforelse
        </table>
    <hr>
    <div class="row" dir="rtl">
        <span class="float-left alert-info" style="
            margin-right: 20px;
            margin-bottom: 20px;
            font-size: 20px;">
            <strong>إجمالي الفواتير : {{$total}} د.ل </strong><br>
        </span>
    </div>
</div>
@endsection
