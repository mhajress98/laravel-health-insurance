@extends('layouts.app')

@section('title')
    تعديل شركة
@endsection

@section('content')
    <div class="container card" dir="rtl">
        <div class="card-header">تعديل شركة</div>
            <div class="card-body">
            <form action="{{ route('company.update',['id'=>$company->id]) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-5">
                <label for="name" class="float-right">إسم الشركة</label>
                <input type="text" name="name" id="name" placeholder="أدخل اسم الشركة" value="{{ $company->name }}" class="form-control">
                @if ($errors->has('name'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-5">
                    <label for="age" class="float-right">نسبة التغطية للتكاليف (المئوية)</label>
                    <input type="text" name="cover_percentage" id="cover_percentage" placeholder="نسبة التغطية للتكاليف" value="{{ $company->cover_percentage }}" class="form-control">
                    @if ($errors->has('cover_percentage'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('cover_percentage') }}</strong>
                        </span>
                    @endif
                        </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-2">
                            <input type="hidden" name="user_id" value="{{Auth::id()}}">
                            <input type="submit" value="حفظ" class="form-control btn btn-primary">
                            </div>
                </div>


            </form>
            </div>
    </div>
@endsection
