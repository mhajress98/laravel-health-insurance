@extends('layouts.app')

@section('title')
    الشركات
@endsection
@section('content')

</div>
<div class="container card" dir="rtl">
        {{-- <a href="{{ route('service.create') }}" class="btn btn-primary">إضافة خدمة</a> --}}
    <table class="table table-striped table-bordered" style="margin-bottom: 0px; margin-top: 10px;">
        <tr>
            <td align="center"><strong>#</strong></td>
            <td align="center"><strong>اسم الشركة</strong></td>
            <td align="center"><strong>نسبة التغطية للتكاليف (المئوية)</strong></td>
            <td align="center"><strong>أُضيفت بواسطة</strong></td>
            <td align="center"><strong>عمليات</strong></td>
        </tr>
        @forelse ($companies as $company)

                    <tr>
                        <td align="center">{{ ++$loop->index }}</td>
                        <td align="center">{{ $company->name }}</td>
                        <td align="center">{{ $company->cover_percentage }} %</td>
                        <td align="center">{{ $company->user->name }}</td>

                        <td align="center">
                            <a href="{{ route('company.edit',['company'=>$company->id]) }}" class=" btn btn-warning">تعديل</a>
                            <a href="{{ route('company.destroy',['company'=>$company->id]) }}" class="btn btn-danger" onclick="return confirm('هل تريد أن تحذف هذه الشركة ؟')">حذف</a>
                        </td>
                    </tr>

                @empty
                    <tr colspan="4">لا توجد شركات</tr>
            @endforelse
        </table>
        <hr>
        <div class="row">
            <div class="col-2">
                <a href="{{ route('company.create') }}" class="btn btn-primary" style="margin-bottom:10px;">إضافة شركة</a>
        </div>
</div>
@endsection
