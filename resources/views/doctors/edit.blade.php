@extends('layouts.app')

@section('title')
    تعديل طبيب
@endsection

@section('content')
    <div class="container card" dir="rtl">
        <div class="card-header">تعديل طبيب</div>
            <div class="card-body">
            <form action="{{ route('doctor.update',['id'=>$doctor->id]) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-5">
                <label for="name" class="float-right">إسم طبيب</label>
                <input type="text" name="name" id="name" placeholder="أدخل اسم الطبيب" value="{{ $doctor->name }}" class="form-control">
                @if ($errors->has('name'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-5">
                    <label for="age" class="float-right">العمر</label>
                    <input type="text" name="age" id="age" placeholder="أدخل عمر الطبيب" value="{{ $doctor->age }}" class="form-control">
                    @if ($errors->has('age'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('age') }}</strong>
                        </span>
                    @endif
                        </div>
                </div>
                <hr>
                    <div class="row">
                        <div class="checkbox">
                            <table class="table" style="margin-right: 15px;">
                                <h5 class="pull-right" style="margin-right: 15px;">الخدمات الذي يقدمها</h5>
                                <tr>
                                    @foreach ($all_services as $service)
                                    <td><input type='checkbox'
                                            {{ in_array($service->id, $doc_services->pluck('id')->toArray()) ? 'checked' : ''}}
                                            name="services[]" value="{{ $service->id }}" />  {{$service->name}}</td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                    </div>
                <hr>
                <div class="col-2">
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                <input type="submit" value="حفظ" class="form-control btn btn-primary">
                </div>

            </form>
            </div>
    </div>
@endsection
