@extends('layouts.app')

@section('title')
    الأطباء
@endsection
@section('content')

<div class="container card" dir="rtl">
        {{-- <a href="{{ route('service.create') }}" class="btn btn-primary">إضافة خدمة</a> --}}
    <table class="table table-striped table-bordered" style="margin-bottom: 0px; margin-top: 10px;">
        <tr>
            <td align="center"><strong>#</strong></td>
            <td align="center"><strong>الاسم</strong></td>
            <td align="center"><strong>العمر</strong></td>
            <td align="center"><strong>أُضيف بواسطة</strong></td>
            <td align="center"><strong>الخدمات الذي يقدمها</strong></td>
            <td align="center"><strong>عمليات</strong></td>
        </tr>
        @forelse ($doctors as $doctor)

                    <tr>
                        <td align="center">{{ ++$loop->index }}</td>
                        <td align="center">{{ $doctor->name }}</td>
                        <td align="center">{{ $doctor->age }}</td>
                        <td align="center">{{ $doctor->user->name }}</td>
                        <td align="center">
                            @foreach ($doctor->services as $service)
                                {{$service->name}} -
                            @endforeach
                        </td>
                        <td align="center">
                            <a href="{{ route('doctor.edit',['doctor'=>$doctor->id]) }}" class=" btn btn-warning">تعديل</a>
                            <a href="{{ route('doctor.destroy',['doctor'=>$doctor->id]) }}" class="btn btn-danger" onclick="return confirm('هل تريد أن تحذف هذا الطبيب ؟')">حذف</a>
                        </td>
                    </tr>

                @empty
                    <tr colspan="4">لا يوجد أطباء</tr>
            @endforelse
        </table>
        <hr>
        <div class="row">
            <div class="col-2">
                <a href="{{ route('doctor.create') }}" class="btn btn-primary" style="margin-bottom:10px;">إضافة طبيب</a>
            </div>
        </div>
</div>
@endsection
