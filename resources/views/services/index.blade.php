@extends('layouts.app')

@section('title')
    الخدمات
@endsection
@section('content')

<div class="container card" dir="rtl">
        {{-- <a href="{{ route('service.create') }}" class="btn btn-primary">إضافة خدمة</a> --}}
    <table class="table table-striped table-bordered" style="margin-bottom: 0px; margin-top: 10px;">
        <tr>
            <td align="center"><strong>#</strong></td>
            <td align="center"><strong>اسم الخدمة</strong></td>
            <td align="center"><strong>السعر</strong></td>
            <td align="center"><strong>أُضيفت بواسطة</strong></td>
            <td align="center"><strong>عمليات</strong></td>
        </tr>
        @forelse ($services as $service)

                    <tr>
                        <td align="center">{{ ++$loop->index }}</td>
                        <td align="center">{{ $service->name }}</td>
                        <td align="center">{{ $service->price }} د.ل</td>
                        <td align="center">{{ $service->user->name }}</td>
                        <td align="center">
                            <a href="{{ route('service.edit',['service'=>$service->id]) }}" class=" btn btn-warning">تعديل</a>
                            <a href="{{ route('service.destroy',['service'=>$service->id]) }}" class="btn btn-danger" onclick="return confirm('هل تريد أن تحذف هذه الخدمة ؟')">حذف</a>
                        </td>
                    </tr>

                @empty
                    <tr colspan="4">لا يوجد خدمات</tr>
            @endforelse
        </table>
        <hr>
        <div class="row">
                <div class="col-2">
                        <a href="{{ route('service.create') }}" class="btn btn-primary" style="margin-bottom:10px;">إضافة خدمة</a>
                </div>
            </div>
</div>
@endsection
