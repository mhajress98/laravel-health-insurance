@extends('layouts.app')

@section('title')
    تعديل خدمة
@endsection

@section('content')
    <div class="container card" dir="rtl">
        <div class="card-header">تعديل خدمة : </div>
            <div class="card-body">
            <form action="{{ route('service.update',['id'=>$service->id]) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-5">
                <label for="name" class="float-right">إسم الخدمة</label>
                <input type="text" name="name" id="name" placeholder="أدخل اسم الخدمة" value="{{ $service->name }}" class="form-control">
                @if ($errors->has('name'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-5">
                    <label for="name" class="float-right">سعر الخدمة</label>
                    <input type="text" name="price" id="price" placeholder="أدخل سعر الخدمة" value="{{ $service->price }}" class="form-control">
                    @if ($errors->has('price'))
                        <span class="float-right alert-danger">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                    @endif
                        </div>
                </div>
                <hr>
                <div class="col-2">
                {{-- <input type="hidden" name="user_id" value="{{Auth::id()}}"> --}}
                <input type="submit" value="حفظ" class="form-control btn btn-primary">
                </div>

            </form>
            </div>
    </div>
@endsection
