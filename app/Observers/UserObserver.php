<?php

namespace App\Observers;

use App\User;
use App\Company;
use App\Doctor;
use App\Patient;
use App\Service;
use App\Invoice;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $patients = $user->patients;
        $services = $user->services;
        $doctors = $user->doctors;
        $invoices = $user->invoices;
        $companies = $user->companies;

        $d_user = User::where('name', 'مستخدم محذوف')->first();
        $id = $d_user->id;

        foreach ($patients as $patient) {
            $p = Patient::find($patient->id);
            $p->user_id = $id ;
            $p->save();
        }

        foreach ($services as $service) {
            $s = Service::find($service->id);
            $s->user_id = $id ;
            $s->save();
        }

        foreach ($doctors as $doctor) {
            $d = Doctor::find($doctor->id);
            $d->user_id = $id ;
            $d->save();
        }

        foreach ($invoices as $invoice) {
            $i = Invoice::find($invoice->id);
            $i->user_id = $id ;
            $i->save();
        }

        foreach ($companies as $company) {
            $c = Patient::find($company->id);
            $c->user_id = $id ;
            $c->save();
        }

    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
