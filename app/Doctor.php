<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    public function invoice_items() {
        return $this->hasMany(Invoice_Item::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function services(){
        return $this->belongsToMany(Service::class, 'doctor_service');
    }
}
