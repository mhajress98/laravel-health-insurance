<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_Item extends Model
{
    protected $table = 'invoice_items';
    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }

    public function doctor() {
        return $this->belongsTo(Doctor::class);
    }

    public function service() {
        return $this->belongsTo(Service::class);
    }
}
