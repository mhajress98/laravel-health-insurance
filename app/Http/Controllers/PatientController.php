<?php

namespace App\Http\Controllers;

use App\Company;
use App\Patient;
use App\Service;
use App\Doctor;
use Illuminate\Http\Request;
use Auth;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all();
        return view('patients.index')->with('patients', $patients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('patients.create')->with('companies', $companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'age' => 'required | numeric',
            'gender' => 'required',
            'blood_type' => 'required'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط',
        ];

        $this->validate($request, $rules, $messages);

        $patient = new Patient;
        $patient->name = $request->input('name');
        $patient->serial_number = time();
        $patient->age = $request->input('age');
        $patient->gender = $request->input('gender');
        $patient->blood_type = $request->input('blood_type');
        $patient->company_id = $request->input('company');
        $patient->user_id = $request->input('user_id');
        $patient->save();

        return redirect()->route('patient.index')->with('success', 'تم إضافة المريض بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
       // $patient = Patient::find($id);
        $companies = Company::all();

        return view('patients.edit')->with(['patient' => $patient, 'companies' => $companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $rules = [
            'name' => 'required',
            'age' => 'required | numeric',
            'gender' => 'required',
            'blood_type' => 'required'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط',
        ];

        $this->validate($request, $rules, $messages);

     //   $patient = Patient::find($id);
        $patient->name = $request->input('name');
        $patient->serial_number = time();
        $patient->age = $request->input('age');
        $patient->gender = $request->input('gender');
        $patient->blood_type = $request->input('blood_type');
        $patient->company_id = $request->input('company');
        $patient->user_id = $request->input('user_id');
        $patient->save();

        return redirect()->route('patient.index')->with('success', 'تم تخزين التعديلات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
   //     $patient = Patient::find($id);
        $patient->delete();

        return redirect()->route('patient.index')->with('success', 'تم إزالة المريض بنجاح');
    }
}
