<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Patient;
use App\Service;
use App\Doctor;
use App\Invoice;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $invoices = Invoice::count();
        $companies =  Company::count();
        $patients = Patient::count();
        $services = Service::count();
        $doctors = Doctor::count();
        return view('home')->with(['companies' => $companies, 'patients' => $patients,
                                   'services' => $services, 'doctors' => $doctors, 'invoices' => $invoices]);
    }
}
