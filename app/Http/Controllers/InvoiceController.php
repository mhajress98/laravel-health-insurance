<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Patient;
use App\Service;
use App\Doctor;
use App\Payment_Method;
use App\Invoice;
use App\Invoice_Item;
use Validator;

use Auth;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();
        return view('invoices.index')->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
     //   $patient = Patient::find($id);
        $services = Service::all();
        $companies =  Company::all();
        $doctors = Doctor::all();
        $payment_methods = Payment_Method::all();

        return view('invoices.create')->with(['companies' => $companies, 'patient' => $patient,
                                   'services' => $services, 'doctors' => $doctors, 'payment_methods' => $payment_methods]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'doctor' => 'required',
            'service' => 'required',
            'payment_method' => 'required'
        ];

        $messages = [
            'required' => 'لقد تركت حقل فارغ',
        ];

        // $this->validate($request, $rules, $messages);

        $validator = Validator::make($request->all() , $rules , $messages);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()->all()], 200);
        }


        $invoice = new Invoice;
        $invoice->patient_id = $request->input('patient_id');
        $invoice->payment_method_id = $request->input('payment_method');
        $invoice->user_id = $request->input('user_id');
        $invoice->total = 0;
        $invoice->save();
        $total = 0;


        foreach($request->doctor as $index => $value){
            $invoice_item = new Invoice_Item;
            $invoice_item->doctor_id = $request->doctor[$index];
            $invoice_item->service_id = $request->service[$index];
            $invoice_item->invoice_id = $invoice->id;
            $invoice_item->save();
            $service = Service::find($request->service[$index]);
            $total += $service->price;
        }
        $invoice->total = $total;
        $invoice->save();

        return redirect()->route('invoice.index')->with('success', 'تم إصدار الفاتورة بنجاح');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
      //  $invoice = Invoice::find($id);
        $invoice->delete();

        return redirect()->route('invoice.index')->with('success', 'تم حذف الفاتورة بنجاح');

    }
}
