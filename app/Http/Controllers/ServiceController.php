<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Auth;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('services.index')->with('services', $services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'name' => 'required',
            'price' => 'required | numeric'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط'
        ];

        $this->validate($request, $rules, $messages);

        $service = new Service;
        $service->name = $request->input('name');
        $service->price = $request->input('price');
        $service->user_id = $request->input('user_id');
        $service->save();

        return redirect()->route('service.index')->with('success', 'تم إضافة الخدمة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
     //   $service = Service::find($id);

        return view('services.edit')->with('service', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {

        $rules = [
            'name' => 'required',
            'price' => 'required | numeric'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط'
        ];

        $this->validate($request, $rules, $messages);

      //  $service = Service::find($id);
        $service->name = $request->input('name');
        $service->price = $request->input('price');
        $service->save();

        return redirect()->route('service.index')->with('success', 'تم تخزين التعديلات');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
       // $service = Service::find($id);
        $service->delete();

        return redirect()->route('service.index')->with('success', 'تم إزالة الخدمة بنجاح');

    }
}
