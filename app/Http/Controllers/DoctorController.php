<?php

namespace App\Http\Controllers;

use App\Service;
use App\Doctor;
use Illuminate\Http\Request;
use Auth;

class DoctorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all();
        return view('doctors.index')->with('doctors' , $doctors);
    }

    public function ajax_res(Doctor $doctor)
    {
       // $doctor = Doctor::find($id);
        $services = $doctor->services;

        return $services;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        return view('doctors.create')->with('services', $services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'age' => 'required | numeric',
            'services' => 'required'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط'
        ];

        $this->validate($request, $rules, $messages);

        $doctor = new Doctor;
        $doctor->name = $request->input('name');
        $doctor->age = $request->input('age');
        $doctor->user_id = $request->input('user_id');
        $doctor->save();
        $doctor->services()->sync($request->services);

        return redirect()->route('doctor.index')->with('success', 'تم إضافة طبيب بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
       // $doctor = Doctor::find($id);
       // $services = Service::all()->except($doctor->services()->pluck('id')->toArray());
      //  $doc_services = $doctor->services()->pluck('service_id')->toArray();
        $doc_services = $doctor->services;
        $all_services = Service::all();

       return view('doctors.edit')->with(['doctor' => $doctor, 'all_services' => $all_services, 'doc_services' => $doc_services]);
        // return view('doctors.edit')->with(['doctor' => $doctor, 'all_services' => $all_services]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
       // dd($request);
        $rules = [
            'name' => 'required',
            'age' => 'required | numeric'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط'
        ];

        $this->validate($request, $rules, $messages);

    //    $doctor = Doctor::find($id);
        $doctor->name = $request->input('name');
        $doctor->age = $request->input('age');
        $doctor->user_id = $request->input('user_id');
        $doctor->save();
        $doctor->services()->sync($request->services);

        return redirect()->route('doctor.index')->with('success', 'تم تخزين التعديلات');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
      //  $doctor = Doctor::find($id);
        $doctor->delete();

        return redirect()->route('doctor.index')->with('success', 'تم إزالة الطبيب بنجاح');
    }
}
