<?php

namespace App\Http\Controllers;

use App\Company;
use App\Service;
use App\Doctor;
use Illuminate\Http\Request;
use Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('companies.index')->with('companies' , $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'cover_percentage' => 'required | numeric | between:0,100'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط',
            'between' => 'يجب ان تكون النسبة بين 0 - 100'
        ];

        $this->validate($request, $rules, $messages);

        $company = new Company;
        $company->name = $request->input('name');
        $company->cover_percentage = $request->input('cover_percentage');
        $company->user_id = $request->input('user_id');
        $company->save();

        return redirect()->route('company.index')->with('success', 'تم إضافة الشركة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
   //     $company = Company::find($id);

        return view('companies.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $rules = [
            'name' => 'required',
            'cover_percentage' => 'required | numeric | between:0,100'
        ];

        $messages = [
            'required' => 'هذا الحقل مطلوب',
            'numeric' => 'يجب أن يحتوي الحقل على أرقام فقط',
            'between' => 'يجب ان تكون النسبة بين 0 - 100'
        ];

        $this->validate($request, $rules, $messages);

     //   $company = Company::find($id);
        $company->name = $request->input('name');
        $company->cover_percentage = $request->input('cover_percentage');
        $company->user_id = $request->input('user_id');
        $company->save();

        return redirect()->route('company.index')->with('success', 'تم تخزين التعديلات');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
    //    $company = Company::find($id);
        $company->delete();

        return redirect()->route('company.index')->with('success', 'تم إزالة الشركة بنجاح');
    }
}
