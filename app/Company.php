<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function patients(){
        return $this->hasMany(Patient::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
