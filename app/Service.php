<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function invoice_items() {
        return $this->hasMany(Invoice_Item::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function doctors() {
        return $this->belongsToMany(Doctor::class, 'doctor_service');
    }
}
