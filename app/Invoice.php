<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function invoice_items() {
        return $this->hasMany(Invoice_Item::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function patient() {
        return $this->belongsTo(Patient::class);
    }

    public function payment_method() {
        return $this->belongsTo(Payment_Method::class);
    }
}
